<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>

    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>

<a href="{{route('push')}}" class="btn btn-outline-primary btn-block">Make a Push Notification!</a>

@auth
    <script src="/enable-push.js" defer></script>
@endauth
</body>
</html>
