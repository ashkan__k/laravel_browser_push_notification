<?php

use App\Http\Controllers\PushController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
//    $user = \App\Models\User::create(
//        [
//            'name' => 'ashkan',
//            'email' => 'ashkan@gmail.com',
//            'password' => \Illuminate\Support\Facades\Hash::make('123'),
//        ]
//    );
    auth()->loginUsingId(1);
    return view('welcome');
});
// Push Notifications Routes
Route::get('/push_page', function () {
    return view('push_notification');
});
Route::post('/push',[PushController::class, 'store']);
Route::get('/push',[PushController::class, 'push'])->name('push');
